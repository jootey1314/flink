import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;


public class WikipediaAnalysis {
    private static StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    public static void main(String[] args) throws Exception {
//        DataStream<String> text = env.socketTextStream("slave3", 9999);
//        AnalysisStreamData(text);

//        ParameterTool tool = ParameterTool.fromArgs(args);
//        int port = tool.getInt("port");


        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> text = env.socketTextStream("slave3", 9999);

        SingleOutputStreamOperator<Tuple2<String, Integer>> dataStream = text.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
                String[] tokens = s.toLowerCase().split("\\W+");

                for (String token : tokens) {
                    if (token.length() > 0) {
                        collector.collect(new Tuple2<>(token, 1));
                    }
                }
            }
        }).keyBy(0).timeWindow(Time.seconds(5)).sum(1);
        dataStream = dataStream;

        dataStream.print();
        // execute program
        env.execute("Java WordCount from SocketTextStream Example");



//       指定算子的方法   Rich functions
        text.flatMap(new RichFlatMapFunction<String, Object>() {
            @Override
            public void flatMap(String s, Collector<Object> collector) throws Exception {
                String[] tokens = s.toLowerCase().split(",");
                for (String token : tokens) {
                    if (token.length() > 0) {

                    }
                }
            }
        });


    }

//    private static void AnalysisStreamData(DataStream<String> dataStream) {
//        SingleOutputStreamOperator<Tuple2<String, Integer>> dataStreams = dataStream.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
//            @Override
//            public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
//                String[] tokens = s.toLowerCase().split("\\w+");
//                for (String token : tokens) {
//                    if (token.length() > 0) {
//                        collector.collect(new Tuple2<>(token, 1));
//                    }
//                }
//            }
//        }).keyBy(0).timeWindow(Time.seconds(5)).sum(1);
//        dataStream.print();
//        try {
//            env.execute("Java WordCount from SocketTextStream Example");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}

