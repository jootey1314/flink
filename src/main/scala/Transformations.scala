import org.apache.flink.api.common.accumulators.LongCounter
import org.apache.flink.api.common.functions.RichMapFunction

import scala.collection.mutable.ListBuffer
import org.apache.flink.api.scala
import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}
import org.apache.flink.configuration.Configuration
import org.apache.flink.core.fs.FileSystem.WriteMode
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment


object Transformations {
  def main(args: Array[String]): Unit = {
    //    mapFunction
    //    FilterFunction
    //    FileterFunction
    //    MapPartitionFunction
    //    groupByFunction
    //    flatmapFun
        joinFun
    //    crossFun   //笛卡尔积
    //    fullJoinFun //相当于left和right
    //    storeFun
    //    counterFunc
    //    DistributedCashe
  }


  private def MapPartitionFunction(): Unit = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val data = buildData()
    val dataParable: DataSet[Int] = env.fromCollection(data).setParallelism(2) //设置分区为5，并行度5个线程

    dataParable.mapPartition(x => {
      print("s")
      x.filter(p => p > 90) //数据被平分成两部分，一部分是1,2,3,4..99,  一部分为2,4,6,8..100
    }).print()


  }

  private def mapFunction = {
    import org.apache.flink.api.scala._
    val data: DataSet[Int] = getData
    data.map(x => (x, 1)).print()
    data.map((_, 2)).print()
  }

  private def FilterFunction = {
    val data = getData
    val value: DataSet[Int] = data.filter(x => x > 5)
    value.print()
  }

  private def getData: scala.DataSet[Int] = {
    import org.apache.flink.api.scala._
    val env: ExecutionEnvironment = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val data = env.fromCollection(List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    data
  }

  private def FileterFunction(): Unit = {
    val data = buildData
    val ints: ListBuffer[Int] = data.filter(x => x + 1 > 3)
    data.map(x => {
      print(x)
    })
  }


  def buildData() = {
    val data = new ListBuffer[Int]()
    for (i <- 1 to 100) {
      data.append(i)
    }
    data
  }


  def initEvr(t: Int): Any = {
    t match {
      case 1 => ExecutionEnvironment.getExecutionEnvironment
      case 2 => StreamExecutionEnvironment.getExecutionEnvironment
    }
  }


  def groupByFunction(): Unit = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val datas = new ListBuffer[(Int, String)]()
    datas.append((1, "java"))
    datas.append((1, "c#"))
    datas.append((1, "c++"))
    datas.append((1, "c"))
    datas.append((2, "scala"))
    datas.append((3, "spark"))
    datas.append((4, "flink"))
    datas.append((4, "hbase"))
    datas.append((4, "redis"))
    datas.append((5, "hive"))

    val data = env.fromCollection(datas)
    //    data.first(3).print()

    data.groupBy(0).first(2).print() //根据第一个字段分组，每个分组获取前两个数据
    println("-----------")
    //根据第一个字段分组，然后在分组内根据第二个字段升序排序，并取出前两个数据
    data.groupBy(0).sortGroup(1,org.apache.flink.api.common.operators.Order.ASCENDING).first(2).print()

  }


  def flatmapFun() = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val data = new ListBuffer[(String)]()

    data.append("spark,scala")
    data.append("flink,scala")

    val envData = env.fromCollection(data)
    //    envData.flatMap(x => x.split(",")).print()
    envData.flatMap(_.split(",")).print()

    //word count
    envData.flatMap(_.split(",")).map((_, 1)).groupBy(0).sum(1).print()

    println("----distinct----")
    envData.flatMap(_.split(",")).distinct().print()
  }


  def joinFun() = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]

    val data = new ListBuffer[(Int, String)]()
    data.append((3, "flink"))
    data.append((1, "spark"))
    data.append((2, "scala"))
    val dataEnv = env.fromCollection(data)


    val data1 = new ListBuffer[(Int, String)]()
    data1.append((1, "C#"))
    data1.append((2, "java"))
    val data1Env = env.fromCollection(data1)


    dataEnv.join(data1Env).where(0).equalTo(0).print()
    println("----------")
    dataEnv.join(data1Env).where(0).equalTo(0).apply((first, secend) => {
      (first._1, first._2, secend._2)
    }).print()
    println("----------")
    dataEnv.leftOuterJoin(data1Env).where(0).equalTo(0).apply((f, l) => {
      if (l == null)
        (f._1, f._2, "-")
      else
        (f._1, f._2, l._1, l._2)
    }).print()

  }


  def crossFun() = {
    import org.apache.flink.api.scala._
    val data = List(1, 2, 3)
    val data1 = List("spark", "c#", "flink")
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val dataEnv = env.fromCollection(data)
    val data1Env = env.fromCollection(data1)

    dataEnv.cross(data1Env).print()
  }

  def fullJoinFun() = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]

    val data = new ListBuffer[(Int, String)]()
    data.append((1, "spark"))
    data.append((2, "scala"))
    data.append((3, "flink"))
    val dataEnv = env.fromCollection(data)


    val data1 = new ListBuffer[(Int, String)]()
    data1.append((1, "C#"))
    data1.append((2, "java"))
    val data1Env = env.fromCollection(data1)

    dataEnv.fullOuterJoin(data1Env).where(0).equalTo(0).apply((first, second) => {
      if (first == null) {
        (second._1, "-", second._2)
      } else if (second == null) {
        (first._1, "-", first._2)
      } else {
        (first._1, first._2, second._2)
      }
    }).print()
  }


  def storeFun() = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    //    val data = 1 to 100;
    val data = List("bb", "ccc")
    val filePath = "E:/test.txt"
    env.fromCollection(data).writeAsText(filePath, writeMode = WriteMode.OVERWRITE)
    env.execute("存储数据")
  }


  def counterFunc() = {
    import org.apache.flink.api.scala._
    val env = ExecutionEnvironment.getExecutionEnvironment
    val data = env.fromElements("hadoop", "spark", "pyspark", "storm")

    val info = data.map(new RichMapFunction[String, String] {
      // step1：定义计数器
      val counter = new LongCounter()

      override def open(parameters: Configuration): Unit = {
        // step2: 注册计数器
        getRuntimeContext.addAccumulator("ele-counts-scala", counter)
      }

      override def map(in: String): String = {
        counter.add(1)
        in
      }
    })
    info.writeAsText("E:/test3", WriteMode.OVERWRITE).setParallelism(4)
    val jobResult = env.execute("CounterApp")
    // step3: 获取计数器
    val num = jobResult.getAccumulatorResult[Long]("ele-counts-scala")
    println("num:" + num)

  }


  //分布式缓存实现
  //  Apache Flink 提供了一个分布式缓存，类似于Hadoop，用户可以并行获取数据。
  //
  //  通过注册一个文件或者文件夹到本地或者远程HDFS等，在getExecutionEnvironment中指定一
  //
  // 个名字就可以。当应用程序执行时，Flink会自动拷贝这个文件或者文件夹到所有worker进程中。用户的Function通过指定的名字可以查找这个文件或者文件夹中的内容。
  def DistributedCashe() = {

    import org.apache.flink.api.scala._

    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]

    val localFilePath = "E:\\test.txt"

    env.registerCachedFile(localFilePath, "pk-scala-dc")

    val info = env.fromElements("spark", "scala", "flink", "hbase")

    info.map(new RichMapFunction[String, String] {
      override def open(parameters: Configuration): Unit = {
        val casheFile = getRuntimeContext.getDistributedCache.getFile("pk-scala-dc")
        val str: String = casheFile.getName()
        println("cache file name:" + str)
      }

      override def map(value: String): String = {
        println(value)
        value
      }
    })

    info.writeAsText("e:/test3", WriteMode.OVERWRITE).setParallelism(4)
    env.execute("distribute cache")

  }

}
