import java.util
import java.util.Properties

import Utilites.SerializeDesSerializeProvider
import com.google.gson.Gson
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, FlinkKafkaProducer}
import org.apache.flink.streaming.util.serialization.SimpleStringSchema

object FlinkKafkaProducer extends BaseFunc {
  private val ZOOKEEPER_HOST = "master:2181,slave3:2181,slave2:2181"
  private val KAFKA_BROKER = "master:9092,slave3:9092,slave2:9092"
  private val TRANSACTION_GROUP = "com.jwp.flink"
  private val TOPIC_NAME = "tt"

  def main(args: Array[String]): Unit = {
    //        produceMessage
    consumMessage
  }


  //  def builderKafkaProducer[T](): FlinkKafkaProducer[T] = {
  //    val pros: Properties = getKafkaProperties
  //    //    val myProducer = new FlinkKafkaProducer011[String](topic2,new KeyedSerializationSchemaWrapper[String](new SimpleStringSchema()), props, FlinkKafkaProducer011.Semantic.EXACTLY_ONCE)
  //
  //    val producer: FlinkKafkaProducer[T] = new FlinkKafkaProducer[T](TOPIC_NAME, new KeyedSerializationSchemaWrapper[T](),
  //      pros, Semantic.EXACTLY_ONCE);
  //    producer
  //  }


  def builderKafkaProducerStr(): FlinkKafkaProducer[String] = {
    val pros: Properties = getKafkaProperties
    val producer: FlinkKafkaProducer[String] = new FlinkKafkaProducer(TOPIC_NAME, new SimpleStringSchema(), pros);
    producer
  }

  //TypedKeyedDeserializationSchema
  def buildKafkaConsumer(): FlinkKafkaConsumer[String] = {
    val pros = getKafkaProperties
    val consumer: FlinkKafkaConsumer[String] = new FlinkKafkaConsumer(TOPIC_NAME, new SimpleStringSchema(), pros)
    //从最早开始消费
    //    consumer.setStartFromEarliest
    consumer.setStartFromTimestamp(1573714229769l)
    consumer
  }

  private def produceMessage = {
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    val data: DataStream[String] = env.addSource(new CustomNonParalleSourceFunction2()).setParallelism(1)
    val producer: FlinkKafkaProducer[String] = builderKafkaProducerStr()
    data.addSink(producer)
    env.execute("kafka topic")
  }

  def consumMessage(): Unit = {
    import org.apache.flink.api.scala._
    val consumer = buildKafkaConsumer()
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val streamMessage: DataStream[String] = env.addSource(consumer)

    //Watermarks,表示最多等待1分钟，业务发生时间超过系统时间1分钟的数据都不进行统计
    streamMessage.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[String](Time.minutes(1)) {
      override def extractTimestamp(t: String): Long = {
        //这里提取时间错，是每个数据上的event时间字段，来判断是否在水位线内，是否触发计算，应该在数据中包含
        0
      }
    })

    //    streamMessage.print()
    streamMessage.map(x => {
      val gson = new Gson
      val model: model = gson.fromJson(x, classOf[model])
      println(s"消费信息：$x")
      println(s"模型信息：${model.addr}")
    })
    env.execute()
  }

  private def getKafkaProperties = {
    val pros = new Properties()
    pros.setProperty("bootstrap.servers", KAFKA_BROKER)
    pros.setProperty("zookeeper.connect", ZOOKEEPER_HOST)
    pros.setProperty("group.id", TRANSACTION_GROUP)
    pros.put("enable.auto.commit", "true")
    pros.put("auto.commit.interval.ms", "10000")
    pros
  }


}


class defineDataSource extends SourceFunction[String] {
  var isRunning = true

  override def run(sourceContext: SourceFunction.SourceContext[String]): Unit = {
    while (isRunning) {
      val list = new util.ArrayList[String]
      list.add("spark")
      list.add("spark2")
      list.add("spark3")
      list.add("spark4")
      val i: Int = scala.util.Random.nextInt(100)
      sourceContext.collect(list.get(i))
      //每2秒产生一条数据
      Thread.sleep(2000)
    }
  }

  override def cancel(): Unit = {
    isRunning = false
  }
}

class CustomNonParalleSourceFunction2 extends SourceFunction[String] {

  var count = 1L
  var isRunning = true

  override def run(sourceContext: SourceFunction.SourceContext[String]): Unit = {
    while (isRunning) {
      val strings = "flink kafka"
      println("message:" + strings)

      val testMap = scala.collection.mutable.Map[String, String]()
      testMap += ("1" -> "2.034")
      testMap += ("2" -> "2.0134")

      val str: String = SerializeDesSerializeProvider.mapToJson(testMap)
      println("map to json:" + str)


      val dataJson: model = model("joo", "man", 10, "cd")
      val map = SerializeDesSerializeProvider.ToJsonStr(dataJson)

      println(s"obj to json:$map")


      sourceContext.collect(map)
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = {
    isRunning = false
  }
}


case class model(name: String, sex: String, age: Int, addr: String)