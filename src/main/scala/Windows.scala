import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.time.Time


object Windows extends BaseFunc {
  def main(args: Array[String]): Unit = {

    import org.apache.flink.api.scala._

    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]

    val data = env.socketTextStream("master", 9990)

    data.flatMap(_.split(",")).map((_, 1)).
      keyBy(0)
      //      .timeWindow(Time.seconds(5)) 滚动窗口
      .timeWindow(Time.seconds(10), Time.seconds(5)) //滑动窗口
      .sum(1)
      .print()
      .setParallelism(1)


    data.flatMap(_.split(",")).map((_, 1)).keyBy(0)

    env.execute("wordCount")
  }
}
