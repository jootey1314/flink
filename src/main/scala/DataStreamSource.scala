import java.{lang, util}

import org.apache.flink.api.scala.{ExecutionEnvironment, _}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.collector.selector.OutputSelector
import org.apache.flink.streaming.api.functions.source.{ParallelSourceFunction, RichParallelSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.api.{EnvironmentSettings, Table}


/**
  * Create by edward
  * On 2019/10/10
  * Description:
  **/

object DataStreamSource {
  def main(args: Array[String]): Unit = {
    val instance = new DataStreamSource
    //    instance.nonParalleSourceFunction()
    //    instance.ParallelSourceFunction
    //    instance.richParallelSourceFunction
    //    instance.unionFunc
    //    instance.splitSelectFun
    instance.StreamingSQL()


  }
}

class DataStreamSource {
  def initEvr(t: Int): Any = {
    t match {
      case 1 => ExecutionEnvironment.getExecutionEnvironment
      case 2 => StreamExecutionEnvironment.getExecutionEnvironment
    }
  }


  def socketFun() = {
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data = env.socketTextStream("127.0.0.1", 888)
    data.print()
    env.execute("socketFun")
  }


  //自定义数据源，此数据源不具备并行计算能力
  def nonParalleSourceFunction(): Unit = {
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data: DataStream[Long] = env.addSource(new CustomNonParalleSourceFunction()).setParallelism(1)
    data.print()
    env.execute()
  }


  def ParallelSourceFunction() = {
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data: DataStream[Long] = env.addSource(new CustomParalleleSourceFunction()).setParallelism(2)
    data.print()
    env.execute()
  }

  def richParallelSourceFunction(): Unit = {
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data: DataStream[Long] = env.addSource(new CustomRichParallelSourceFunction).setParallelism(1)

    data.map(x => {
      println("received:" + x)
      x
    }).filter(_ != "").print().setParallelism(1)

    //    data.print()
    env.execute()
  }


  def unionFunc() = {
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data1 = env.addSource(new CustomNonParalleSourceFunction)
    val data2 = env.addSource(new CustomNonParalleSourceFunction)
    val datas: DataStream[Long] = data1.union(data2)
    datas.map(x => {
      println(x)
      x
    })
  }

  def splitSelectFun() = {
    println("splitFun")
    import org.apache.flink.api.scala._
    val env = initEvr(2).asInstanceOf[StreamExecutionEnvironment]
    val data = env.addSource(new CustomNonParalleSourceFunction)


    val split = data.split(new OutputSelector[Long] {
      override def select(out: Long): lang.Iterable[String] = {
        val list = new util.ArrayList[String]()
        if (out != 0) {
          list.add("even")
        }
        else {
          list.add("old")
        }
        list
      }
    })
    split.select("odd", "even").print().setParallelism(1)

  }


  def StreamingSQL() = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //    val tEnv = StreamTableEnvironment.create(env)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val settings = EnvironmentSettings.newInstance()
      .useBlinkPlanner()
      .inStreamingMode()
      .build()
    //    val tEnv = StreamTableEnvironment.create(env, settings)
    val tEnv = StreamTableEnvironment.create(env)

    val orderA: DataStream[Order] = env.fromCollection(Seq(
      Order(1L, "beer", 3),
      Order(1L, "diaper", 4),
      Order(3L, "rubber", 2)))

    val orderB: DataStream[Order] = env.fromCollection(Seq(
      Order(2L, "pen", 3),
      Order(2L, "rubber", 3),
      Order(4L, "beer", 1)))

    // convert DataStream to Table      流表操作方法一  'ts.rowtime
    val tableA: Table = tEnv.fromDataStream(orderA, 'user, 'product, 'amount)
    // register DataStream as Table     流表操作方法二   'ts.rowtime
    tEnv.registerDataStream("OrderB", orderB, 'user, 'product, 'amount)


    val result: Table = tEnv.sqlQuery(
      s"""
      select * from $tableA where amount>2
      UNION ALL
      SELECT * FROM OrderB where amount<2
      """.stripMargin)
    result.toAppendStream[Order].print()
    env.execute()
  }


}

case class Order(user: Long, product: String, amount: Int)


//实现SourceFunction   SourceFunction
class CustomNonParalleSourceFunction extends SourceFunction[Long] {
  var count = 1L
  var isRunning = true

  override def run(sourceContext: SourceFunction.SourceContext[Long]): Unit = {
    while (isRunning) {
      sourceContext.collect(count)
      count += 1
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = {
    isRunning = false
  }
}


//ParallelSourceFunction

class CustomParalleleSourceFunction extends ParallelSourceFunction[Long] {
  var isRunning = true
  var count = 1L

  override def run(sourceContext: SourceFunction.SourceContext[Long]): Unit = {
    while (isRunning) {
      sourceContext.collect(count)
      count += 1
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = {
    isRunning = false
  }
}


//RichParallelSourceFunction
class CustomRichParallelSourceFunction extends RichParallelSourceFunction[Long] {
  var isRunning = true
  var count = 1L

  override def run(sourceContext: SourceFunction.SourceContext[Long]): Unit = {
    while (isRunning) {
      sourceContext.collect(count)
      count += 1
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = {
    isRunning = false
  }
}
