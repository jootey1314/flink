import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


//自定义sink，将socket接受到的数据出入到mysql中，主要是重写RichSinkFunction

public class SinkToMysql extends RichSinkFunction<Student> {

    PreparedStatement ps;
    private Connection con;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        con = getCon();
        String sql = "insert into student(id,name,age) values(?,?,?);";
        ps = this.con.prepareStatement(sql);
    }

    @Override
    public void invoke(Student value, Context context) throws Exception {
        ps.setInt(1, value.getId());
        ps.setString(2, value.getName());
        ps.setInt(3, value.getAge());
        ps.executeUpdate();
    }

    @Override
    public void close() throws Exception {
        super.close();
        if (con != null) {
            con.close();
        }
        if (ps != null) {
            ps.close();
        }
    }


    private static Connection getCon() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String conStr = "jdbc:mysql://master:3306/imooc_flink?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false";
            con = DriverManager.getConnection(conStr, "hive", "123456");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("mysql get connection has exception");
        }
        return con;
    }
}


class Student {
    private int id;
    private String name;
    private int age;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


class call {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> source = env.socketTextStream("master", 9990);
        SingleOutputStreamOperator<Student> studentStream = source.map((MapFunction<String, Student>) s -> {
            String[] splits = s.split(",");
            Student student = new Student();
            student.setId(Integer.parseInt(splits[0]));
            student.setAge(Integer.parseInt(splits[2]));
            student.setName(splits[1]);
            return student;
        });
        studentStream.addSink(new SinkToMysql());
        env.execute("dd");

    }
}