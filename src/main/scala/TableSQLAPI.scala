import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.Table
import org.apache.flink.table.api.scala.BatchTableEnvironment
import org.apache.flink.types.Row


object TableSQLAPI {
  def main(args: Array[String]): Unit = {
    getDataSet()
  }


  def getDataSet() = {
    import org.apache.flink.api.scala._
    val env = initEvr(1).asInstanceOf[ExecutionEnvironment]
    val path = "e:/test/sales.csv"
    val dataSet_csv = env.readCsvFile[SalesLog](path, ignoreFirstLine = true)



    val tableEnv = BatchTableEnvironment.create(env)
    val tableData = tableEnv.fromDataSet(dataSet_csv)
    tableEnv.registerTable("sales", tableData)
    val resultTable: Table = tableEnv.sqlQuery("select customerId, sum(amountPaid) money from sales group by customerId")
    tableEnv.toDataSet[Row](resultTable).print()

  }


  def initEvr(t: Int): Any = {
    t match {
      case 1 => ExecutionEnvironment.getExecutionEnvironment
      case 2 => StreamExecutionEnvironment.getExecutionEnvironment
    }
  }
}

case class SalesLog(transactionId: String, customerId: String, itemId: String, amountPaid: Double)