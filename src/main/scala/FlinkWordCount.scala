import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time

object FlinkWordCount {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //    wordCount(env)

    val env2 = ExecutionEnvironment.getExecutionEnvironment
    //    fromCollection(env2)


    //    readFromTxt(env2)
    readFromCsv(env2)
  }


  private def wordCount(env: StreamExecutionEnvironment) = {
    // 引入隐式转换
    import org.apache.flink.api.scala._

    val text = env.socketTextStream("master", 9990)


    //    text.flatMap(_.split(","))
    //      .map((_, 1))
    //      .keyBy(0)
    //      .timeWindow(Time.seconds(5))
    //      .sum(1)
    //      .print()
    //      .setParallelism(1)


    text.flatMap(_.split(","))
      .map(x => WordWithCount(x, 1))
      .keyBy("word").timeWindow(Time.seconds(5))
      .sum("count")
      .print()
      .setParallelism(1)
    env.execute("StreamingWCScalaApp")
  }

  def fromCollection(evn: ExecutionEnvironment): Unit = {
    import org.apache.flink.api.scala._
    //    #本地数据集
    val data = 1 to 10
    evn.fromCollection(data).print()
  }


  private def readFromTxt(env: ExecutionEnvironment): Unit = {
    env.readTextFile("E:\\Source\\Flink\\src\\main\\resources\\hello.txt").print()
  }

  private def readFromCsv(env: ExecutionEnvironment): Unit = {
    val path = "E:\\Source\\Flink\\src\\main\\resources\\nums.csv"
    env.readCsvFile[(String, String, String)](path, ignoreFirstLine = true).print()
    env.readCsvFile[CsvModel](path, ignoreFirstLine = true).print()
  }

}

/** Data type for words with count */
case class WordWithCount(word: String, count: Long)

case class CsvModel(num0: String, word: String, num1: String)



































