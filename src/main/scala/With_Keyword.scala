


object With_Keyword {
  def main(args: Array[String]): Unit = {

    //在没有with的情况下，会先执行父类，这时因为子类override的存在weight还会用覆盖的值，但此时后没有经过子类进行覆盖赋值，
    // 默认weight为0，所以doubleWeight为0，等父类执行完成后在执行子类，所以最后weight为150；
    val dog = new dog
    println(dog.wigth)
    println(dog.higwight)


    //因为子类中没有对父类进行覆盖及其它操作，所以最后都是父类的值，即weight为100，doubleWeight为200。
    println("-----------------------------------------")
    val dog1 = new dog1
    println(dog1.wigth)
    println(dog1.higwight)


    //with在子类定义完之后，先执行子类再执行父类，这时父类中的方法会自动接收覆盖后的属性即weight为150，doubleWeight为300
    println("-----------------------------------------")
    val dog2 = new dog2
    println(dog2.wigth)
    println(dog2.higwight)
  }
}


class animal {
  val wigth = 100
  val higwight = 2 * wigth
}


class dog extends animal {
  override val wigth = 150
}


class dog1 extends animal {
}

class dog2 extends {
  override val wigth = 150
} with animal


